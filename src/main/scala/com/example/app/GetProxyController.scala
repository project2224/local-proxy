package com.example.app
import org.scalatra._
import sttp.client3._
import sttp.model.Uri
import java.net.URI

import scala.concurrent.ExecutionContext

class GetProxyController() extends ScalatraServlet with FutureSupport {

  protected implicit def executor: ExecutionContext = scala.concurrent.ExecutionContext.global

  get("/*") {
    val uriString = s"http://localhost:8081${request.getRequestURI}?${request.getQueryString}"
    val jUri = new URI(uriString)
    val sUri = Uri(jUri)
    val requestToForward = basicRequest.get(sUri)
      val backend = HttpURLConnectionBackend()
      val response = requestToForward.send(backend)
    val sHeaders = response.headers
    val traHeadersSeq = sHeaders.map(header => header.name -> header.value)
    val traHeadersMap = traHeadersSeq.toMap
      response.body match {
        case Left(bad) => ActionResult(response.code.code, bad, traHeadersMap)
        case Right(good) => ActionResult(response.code.code, good, traHeadersMap)
      }
  }

}